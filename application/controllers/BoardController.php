<?php

namespace application\controllers;

// index 함수의 매개변수를 받기 위해 Controller를 상속
class BoardController extends Controller {
	public function writeView($category, $idx, $pageNo) {
		require_once 'application/views/board/write.php';
	}
	public function index($category, $idx, $pageNo) {
		// 네임스페이스를 통해 BoardModel() 객체 생성
		$model = new \application\models\BoardModel();
		// $list 변수에 selectList 함수 결과를 할당
		$list = $model->selectList($category, $idx, $pageNo);
		//view 파일중 index.php 파일을 호출
		require_once 'application/views/board/index.php';
	}
	public function insertBoard($category, $idx, $pageNo) {
		$model = new \application\models\BoardModel();
		if (isset($_POST['submit_insert_board'])) {
			$model->insertBoard($_POST['title'], $_POST['content']);
		}

		header('location:/application/board/index');
	}
}
