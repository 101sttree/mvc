<?php

namespace application\controllers;

class Controller {
    // 생성자 함수
	public function __construct($menu, $action, $category, $idx, $pageNo) {
		if (!file_exists(_ROOT . '/application/application/models/' . $menu . 'Model.php')) {
			var_dump(
				'Model Class not found.(application/models/' . $menu . 'Model.php)'
			);
			exit();
		}
        // 매개변수로 받아온 action에 인자로 다른 매개변수들을 전달해서 실행한다.
        // BoardController로 추정
		$this->$action($category, $idx, $pageNo);
	}
}
