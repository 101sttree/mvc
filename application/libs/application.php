<?php

namespace application\libs;

class Application {
	public $controller;
	public $action;

	public function __construct()
	{
		$getUrl = '';
        //get으로 url 이라는 값이 전달됬을 경우
		if (isset($_GET['url'])) {
            // 가장 오른쪽 /(슬래시)를 제거
			$getUrl = rtrim($_GET['url'], '/');
            //url에 사용할 수 있는 문자를 제외하고 모두 삭제
			$getUrl = filter_var($getUrl, FILTER_SANITIZE_URL);
		}
        //슬래시를 기준으로 나누어 배열로 만듬
		$getParams = explode('/', $getUrl);
        // url 경로 하나하나를 확인하고 기본값을 줌
        // 축약형 if 문
        // url의 첫번째 원소가 존재하고 공백이 아닐 경우 0번째 원소의 값을 그대로 할당, 조건이 하나라도 거짓이면 board를 할당
		$params['menu']     = isset($getParams[0]) && $getParams[0] != '' ? $getParams[0] : 'board';
		$params['action']   = isset($getParams[1]) && $getParams[1] != '' ? $getParams[1] : 'index';
		$params['category'] = isset($getParams[2]) && $getParams[2] != '' ? $getParams[2] : null;
		$params['idx']      = isset($getParams[3]) && $getParams[3] != '' ? $getParams[3] : null;
		$params['pageNum']  = isset($getParams[4]) && $getParams[4] != '' ? $getParams[4] : null;

        // 해당 경로와 파일이 존재하지 않을 경우 오류 메세지 출력
		if (!file_exists('application/controllers/' . $params['menu'] . 'Controller.php')) {

			echo 'application/controllers/' . $params['menu'] . 'Controller.php';
			echo '해당 컨트롤러가 존재하지 않습니다.';
			exit();
		}
        // 앞의 if 문에서 프로그램이 종료되지 않았을 경우 controllerName에 경로값을 할당
		$controllerName =
			'\application\controllers\\' . $params['menu'] . 'controller';
        // 경로값에 해당하는 객체의 인스턴스를 만들고 매개변수로 url의 경로값들을 전달
		new $controllerName(
			$params['menu'],
			$params['action'],
			$params['category'],
			$params['idx'],
			$params['pageNum']
		);
	}
}
