<?php

//new를 통해 객체를 생성할때 객체의 경로를 $path 로 받아옴
spl_autoload_register(function ($path) {
	//받아오는 경로의 구분자가 역슬래시로 되어있기 때문에 슬래시로 변경
	$path = str_replace('\\', '/', $path);
	//슬래시를 구분자로 경로 하나하나를 원소로 가진 배열로 만듬
	$paths = explode('/', $path);
	//preg_match를 통한 정규식 검사
	//strtolower로 경로의 대문자를 소문자로 변경, 0번째 원소는 최상위 디렉토리이기 때문에 1번째 원소를 비교,
	//경로에 model 이라는 문자열이 포함되있는지 확인
	if (preg_match('/model/', strtolower($paths[1]))) {
		$className = 'models';

	//경로에 controller 이라는 문자열이 포함되있는지 확인
	} else if (preg_match('/controller/', strtolower($paths[1]))) {
		$className = 'controllers';
	//둘 다 아닐경우		
	} else {
		$className = 'libs';
	}
	//최종 경로는 최상위디렉토리/정규식에 의해 할당된 값/파일명.php
	$loadpath = $paths[0] . '/' . $className . '/' . $paths[2] . '.php';

	//echo 'autoload $path : ' . $loadpath . '<br>';
	//최종 경로에 해당하는 파일이 없을 경우 오류 메세지 출력
	if (!file_exists($loadpath)) {
		echo " --- autoload : file not found. ($loadpath) ";
		exit();
	}
	//최종 경로에 해당하는 파일을 불러옴
	require_once $loadpath;
});
